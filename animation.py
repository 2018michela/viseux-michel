from codingweeks import *

#Fonctionnalité 7



def animer(univers):
    fig=plt.figure()
    im=plt.imshow(univers,cmap="Greys",animated=True)
    def animate(i):#fonction qui sera itérée par FuncAnimation
        univers=im.get_array()
        im.set_array(generation(univers))#permet de passer a la generation suivante
        return im,
    global anim
    anim=animation.FuncAnimation(fig,animate,interval=500,frames=10,blit=True)
    plt.show()

#animer(Beacon)

#Fonctionnalité 8

def wanted_animate(universe_size,seed,interval_time,color_of_game,x_start,y_start,save):
    graine = np.array(seed)#donne l'amorce voulue
    univers_vierge=generate_universe(universe_size)#genere l'univers vierge initial
    univers=add_seed_to_universe(graine,univers_vierge,x_start,y_start)#ajoute l'amorce dans l'univers vierge
    fig=plt.figure()
    im=plt.imshow(univers,cmap= color_of_game ,animated=True)
    def animate(i):
        univers=im.get_array()
        im.set_array(generation(univers))
        return im,
    global anim
    if interval_time==None:#affecte 300ms pour l'intervalle de temps si rien n'est specifié(None)
        anim=animation.FuncAnimation(fig,animate,interval=300,frames=10,blit=True)
    else:
        anim=animation.FuncAnimation(fig,animate,interval=interval_time,frames=10,blit=True)
    if save:
        animation.Animation.save('game_of_life.mp4')
    plt.show()

#wanted_animate((18,18),[[0,0,0,0],[0,1,1,1],[0,0,0,0],[0,0,0,0]],500,"Greys",2,2)
