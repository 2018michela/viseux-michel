from codingweeks import *

def main():
    graine = np.array([[1,0,1],[0,0,1],[1,0,1]])#donne l'amorce voulue
    univers_vierge=generate_universe((7,7))#genere l'univers vierge initial
    univers=add_seed_to_universe(graine,univers_vierge,1,1)
    fig=plt.figure()
    im=plt.imshow(univers,cmap="Greys",animated=True)
    def animate(i):
        univers=im.get_array()
        im.set_array(generation(univers))
        return im,
    global anim
    anim=animation.FuncAnimation(fig,animate,interval=500,frames=10,blit=True)
    plt.show()

if __name__=="__main__":
    main()
