from codingweeks import *



def test_generate_universe():
    assert generate_universe((4,4)) == [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]


def test_create_seed():
    seed=create_seed(type_seed = "r_pentomino")
    assert seed==[[0, 1, 1], [1, 1, 0], [0, 1, 0]]


def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()


def animer(univers):
    plt.imshow(univers)
    fig=plt.figure()
    im=plt.imshow(univers,cmap="Greys",animated=True)
    def animate(i):
        print("azazfze")
        univers=im.get_array()
        im.set_array(generation(univers))
        return im,
    global anim
    anim=animation.FuncAnimation(fig,animate,interval=500,frames=10,blit=True)
    plt.show()
