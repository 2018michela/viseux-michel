#FICHIER PRINCIPAL CONTENANT TOUS LE SCRIPT
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 15:00:03 2018

@author: alexismichel
"""

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import random

#fonctionnalité 1

def generate_universe(size):
    #permet de créer l'univers
    universe = []
    for i in range(0,size[0]):
        p = []
        for j in range(0,size[1]):
            p.append (0)
        universe.append(p)
    return universe

r_pentomino =[[0, 1, 1], [1, 1, 0], [0, 1, 0]]

def create_seed(type_seed):
    if type_seed=="r_pentomino":
        return [[0, 1, 1], [1, 1, 0], [0, 1, 0]]
    #ici on renvoie la seule amorce connue pour le moment, sinon on ne renvoie rien



def add_seed_to_universe(seed,universe,x_start,y_start):
    universe=np.array(universe,dtype=np.uint8)
    seed=np.array(seed,dtype=np.uint8)
    universe[y_start:len(seed)+y_start , x_start:len(seed)+x_start]=seed
    #remplace l'univers par la graine choisie en commencant a la ligne y_start et la colonne x_start
    plt.show()
    return universe



#fonctionnalité 2

L=np.array([[1,0,0,0,0],[0,0,0,0,0],[1,0,1,0,1],[0,0,0,0,0],[0,0,0,0,0]])
Beacon=np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,1,0,0,0],[0,0,0,1,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]])

def representer_univers(univers):
    L2=np.zeros((len(univers),len(univers[0]),3))
    for i in range(len(univers)):
        for j in range(len(univers[i])):
            if univers[i][j]==1:
                L2[i][j]=[0,0,0]
            else:
                L2[i][j]=[255,255,255]
    #permet de representer l'univers en noir et blanc
    plt.imshow(L2)
    #plt.imshow((univers*255).astype(np.uint8))
    #plt.show()



#fonctionnalité 3

#catalogue des amorces usuelles
seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "block": [[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]],
    "blinker": [[0,0,0,0,0],[0,0,0,0,0],[0,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0]],
    "tub": [[0,0,0,0,0],[0,0,1,0,0],[0,1,0,1,0],[0,0,1,0,0],[0,0,0,0,0]],
    "beehive": [[0,0,0,0,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,0,0,0,0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}



class PasDansLeDico(Exception): pass


def choix_amorce_alea():
    d=sorted(seeds.items(), key=lambda t: t[0])
    a=random.randint(0,9)
    return d[a][1]
    #on choisit une amorce aléatoire dans le catalogue


def choix_amorce(type_size):
    if type_size in seeds.keys():
        return seeds.get(type_size)
    else:
        raise PasDansLeDico
    #onc choisit une amorce parmi celles proposées dans le catalogue



class UniversTropPetit(Exception): pass

def init_universe(size):
    graine=choix_amorce_alea()
    vertical=len(graine)
    horizontal=len(graine[0])
    if size[0]<vertical or size[1]<horizontal:
        raise UniversTropPetit
    else:
        return generate_universe(size)
    #permet de vérifier que l'univers est suffisamment grand pour la graine choisie


#fonctionnalité 4

def survival(universe,x,y):
    v=0
    m=0  #nombre de voisines vivantes v et mortes m
    
    if x==len(universe)-1 and y!=len(universe[0])-1:
        I=[[x-1,y-1],[x,y-1],[0,y-1],[0,y],[0,y+1],[x,y+1],[x-1,y+1],[x-1,y]] #x+1 devient 0
    elif y==len(universe[0])-1 and x!=len(universe)-1:
        I=[[x-1,y-1],[x,y-1],[x+1,y-1],[x+1,y],[x+1,0],[x,0],[x-1,0],[x-1,y]] #y+1 devient 0
    elif x==len(universe)-1 and y ==len(universe[0])-1:
        I=[[x-1,y-1],[x,y-1],[0,y-1],[0,y],[0,0],[x,0],[x-1,0],[x-1,y]]
    else:
        I=[[x-1,y-1],[x,y-1],[x+1,y-1],[x+1,y],[x+1,y+1],[x,y+1],[x-1,y+1],[x-1,y]] 
        #on a listé les indices des voisins, en faisant attention aux cas limite aux bords de l'univers
   
    for i in I:
        if universe[i[0]][i[1]]==0:
            m+=1
        else:
            v+=1
    if universe[x][y]==0: #cellule morte
        if v==3:
            return 1 #3 voisins vivants donc la cellule renaît
        else:
            return 0
    if universe[x][y]==1:
        if v!=2 and v!=3:
            return 0 #la cellule meurt
        else:
            return 1
    

#fonctionnalité 5

def generation(universe):
    universe2=np.zeros((len(universe),len(universe[0]))) # copie pour eviter effets de bords
    for x in range(len(universe)):
        for y in range(len(universe[0])):
           universe2[x][y]= survival(universe,x,y)
    return universe2

#fonctionnalité 6

def game_life_simulate(univers,k): #k est le nombre d'itérations
    L=univers
    for i in range(k):
        L=generation(L)
    return L

#print(representer_univers(game_life_simulate(L,4)))

#fonctionnalité 7

def animer(univers):
    fig=plt.figure()#initialisation de la figure
    im=plt.imshow(univers,cmap="Greys",animated=True)
    def animate(i):#fonction qui sera iteree par FuncAnimation
        univers=im.get_array()
        im.set_array(generation(univers))#permet de passer a la generation suivante pour pouvoir l'afficher
        return im,
    global anim
    anim=animation.FuncAnimation(fig,animate,interval=500,frames=10,blit=True)
    plt.show()

#animer(Beacon)


#Fonctionnalité 8

def wanted_animate(universe_size,seed,interval_time,color_of_game,x_start,y_start):
    graine = np.array(seed)#donne l'amorce voulue
    univers_vierge=generate_universe(universe_size)#genere l'univers vierge initial
    univers=add_seed_to_universe(graine,univers_vierge,x_start,y_start)#ajoute l'amorce dans l'univers vierge
    fig=plt.figure()
    im=plt.imshow(univers,cmap= color_of_game ,animated=True)
    def animate(i):
        univers=im.get_array()
        im.set_array(generation(univers))
        return im,
    global anim
    if interval_time==None:#affecte 300ms pour l'intervalle de temps si rien n'est specifié(None)
        anim=animation.FuncAnimation(fig,animate,interval=300,frames=10,blit=True)
    else:
        anim=animation.FuncAnimation(fig,animate,interval=interval_time,frames=10,blit=True)
    plt.show()

#wanted_animate((18,18),[[1,1,0,0],[0,1,1,1],[0,1,1,1],[1,1,0,0]],500,"Greys",2,2)
