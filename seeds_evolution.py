from codingweeks import *


#fonctionnalité 4

def survival(universe,x,y):
    v=0
    m=0  #nombre de voisines vivantes v et mortes m

    if x==len(universe)-1 and y!=len(universe[0])-1:
        I=[[x-1,y-1],[x,y-1],[0,y-1],[0,y],[0,y+1],[x,y+1],[x-1,y+1],[x-1,y]] #x+1 devient 0
    elif y==len(universe[0])-1 and x!=len(universe)-1:
        I=[[x-1,y-1],[x,y-1],[x+1,y-1],[x+1,y],[x+1,0],[x,0],[x-1,0],[x-1,y]] #y+1 devient 0
    elif x==len(universe)-1 and y ==len(universe[0])-1:
        I=[[x-1,y-1],[x,y-1],[0,y-1],[0,y],[0,0],[x,0],[x-1,0],[x-1,y]]
    else:
        I=[[x-1,y-1],[x,y-1],[x+1,y-1],[x+1,y],[x+1,y+1],[x,y+1],[x-1,y+1],[x-1,y]]
        #on a listé les indices des voisins, en faisant attention aux cas limite aux bords de l'univers

    for i in I:
        if universe[i[0]][i[1]]==0:
            m+=1
        else:
            v+=1
    if universe[x][y]==0: #cellule morte
        if v==3:
            return 1 #3 voisins vivants donc la cellule renaît
        else:
            return 0
    if universe[x][y]==1:#cellule vivante
        if v!=2 and v!=3:
            return 0 #la cellule meurt si elle n'a pazs 2 ou 3 voisins vivants
        else:
            return 1#remarque: on renvoit l'état mais on ne le modifie pas dans l'univers pour eviter effets de bords


#fonctionnalité 5

def generation(universe):
    universe2=np.zeros((len(universe),len(universe[0]))) # copie pour eviter effets de bords
    for x in range(len(universe)):
        for y in range(len(universe[0])):
           universe2[x][y]= survival(universe,x,y)
    return universe2

#fonctionnalité 6

def game_life_simulate(univers,k): #k est le nombre d'itérations
    L=univers
    for i in range(k):
        L=generation(L)
    return L


