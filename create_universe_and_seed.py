from codingweeks import *

def generate_universe(size):
    #permet de créer l'univers
    universe = []
    for i in range(0,size[0]):
        p = []
        for j in range(0,size[1]):
            p.append (0)
        universe.append(p)
    return universe

r_pentomino =[[0, 1, 1], [1, 1, 0], [0, 1, 0]]

def create_seed(type_seed):
    if type_seed=="r_pentomino":
        return [[0, 1, 1], [1, 1, 0], [0, 1, 0]]
    #ici on renvoie la seule amorce connue pour le moment, sinon on ne renvoie rien



def add_seed_to_universe(seed,universe,x_start=1,y_start=1):
    universe=np.array(universe,dtype=np.uint8)
    seed=np.array(seed,dtype=np.uint8)
    universe[y_start:len(seed)+1 , x_start:len(seed)+1]=seed
    #remplace l'univers par la graine choisie en commencant a la ligne y_start et la colonne x_start
    return universe

#Fonctionnalité 2

L=np.array([[1,0,0,0,0],[0,0,0,0,0],[1,0,1,0,1],[0,0,0,0,0],[0,0,0,0,0]])
Beacon=np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,1,0,0,0],[0,0,0,1,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]])

def representer_univers(univers):
    L2=np.zeros((len(univers),len(univers[0]),3))
    for i in range(len(univers)):
        for j in range(len(univers[i])):
            if univers[i][j]==1:
                L2[i][j]=[0,0,0]
            else:
                L2[i][j]=[255,255,255]
    #permet de representer l'univers en noir et blanc
    #on pouvait aussi utiliser plt.imshow(univers,cmap="Greys")



#fonctionnalité 3

#catalogue des amorces usuelles
seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "block": [[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]],
    "blinker": [[0,0,0,0,0],[0,0,0,0,0],[0,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0]],
    "tub": [[0,0,0,0,0],[0,0,1,0,0],[0,1,0,1,0],[0,0,1,0,0],[0,0,0,0,0]],
    "beehive": [[0,0,0,0,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,0,0,0,0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}



class PasDansLeDico(Exception): pass


def choix_amorce_alea():
    d=sorted(seeds.items(), key=lambda t: t[0])
    a=random.randint(0,9)
    return d[a][1]
    #on choisit une amorce aléatoire dans le catalogue


def choix_amorce(type_size):
    if type_size in seeds.keys():
        return seeds.get(type_size)
    else:
        raise PasDansLeDico
    #onc choisit une amorce parmi celles proposées dans le catalogue



class UniversTropPetit(Exception): pass

def init_universe(size):
    graine=choix_amorce_alea()
    vertical=len(graine)
    horizontal=len(graine[0])
    if size[0]<vertical or size[1]<horizontal:
        raise UniversTropPetit
    else:
        return generate_universe(size)
    #permet de vérifier que l'univers est suffisamment grand pour la graine choisie
